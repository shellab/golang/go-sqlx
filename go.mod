module go-sqlx

go 1.20

require (
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.2.0
	github.com/sanity-io/litter v1.5.5
)
