package main

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/sanity-io/litter"
)

type User struct {
	ID        int       `db:"user_id"`
	User      string    `db:"username"`
	Pass      string    `db:"password"`
	Email     string    `db:"email"`
	CreatedAt time.Time `db:"created_on"`
	LastLogin time.Time `db:"last_login"`
}

func main() {
	dsn := fmt.Sprintf("host=%s user=%s password=%s port=%d dbname=%s sslmode=disable",
		"127.0.0.1",
		"root",
		"root",
		5432,
		"dachi",
	)
	db, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		fmt.Println(err)
		return
	}

	user := User{}
	/* Get, Select 是 Query 簡單寫法 */
	// take row
	err = db.Get(&user, "select * from accounts where user_id=$1", 4)
	if err != nil {
		fmt.Println(err)
		return
	}
	litter.Dump(user)
	fmt.Println(user.CreatedAt.Format(time.UnixDate))

	// query rows
	users := []User{}
	err = db.Select(&users, "select * from accounts limit 10")
	if err != nil {
		fmt.Println(err)
		return
	}
	for i, user := range users {
		fmt.Printf("%d id=%d %s %s %s\n", i, user.ID, user.User, user.Pass, user.Email)
	}

	// insert
	now := time.Now()
	r, err := db.Exec("insert into accounts (username,password,email,created_on,last_login) values ($1, $2, $3, $4, $5)", "sqlx", "sqlpass", "aa@aa.cc", now, now)
	if err != nil {
		fmt.Println(err)
		return
	}
	affected, err := r.RowsAffected()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("affected rows ", affected)

	// update
	_, err = db.Exec("update accounts set password=$1 where username=$2", "1234", "sqlx")
	if err != nil {
		fmt.Println(err)
		return
	}

	// delete
	r, err = db.Exec("delete from accounts where username=$1", "sqlx")
	if err != nil {
		fmt.Println(err)
		return
	}
	affected, _ = r.RowsAffected()
	fmt.Println("delete affected rows ", affected)

	// Named Exec and query
	// named query
	// args := map[string]interface{}{
	// 	"user": "%user%",
	// }
	args := User{
		User: "%user%",
	}
	rs, err := db.NamedQuery("select * from accounts where username like :username", args)
	if err != nil {
		fmt.Println(err)
		return
	}
	users2 := []User{}
	for rs.Next() {
		user := User{}
		err := rs.StructScan(&user)
		if err != nil {
			fmt.Println(err)
			return
		}
		users2 = append(users2, user)
	}
	litter.Dump(users2)
	//named exec
	if err = insertUser(db); err != nil {
		fmt.Println(err)
	}

	// in query
	if err = queryIn(db); err != nil {
		fmt.Println(err)
	}
}

func queryIn(db *sqlx.DB) error {

	users := []string{
		"user1",
		"user2",
	}
	query, args, err := sqlx.In("select * from accounts where username in (?)", users)
	if err != nil {
		return err
	}
	query = db.Rebind(query) // ?, ?, ? => $1, $2, $3

	fetchUsers := []User{}
	err = db.Select(&fetchUsers, query, args...)
	if err != nil {
		return err
	}
	litter.Dump(fetchUsers)

	return nil
}

func insertUser(db *sqlx.DB) error {
	tx, err := db.Beginx()

	if err != nil {
		return err
	}
	newUsers := []User{
		{
			User:  "test",
			Pass:  "7788",
			Email: "aa",
		},
		{
			User:  "test2",
			Pass:  "9966",
			Email: "bb",
		},
	}
	r, err := tx.NamedExec("insert into accounts (username,password,email,created_on,last_login) values (:username, :password, :email, :created_on, :last_login)", newUsers)
	if err != nil {
		tx.Rollback()
		return err
	}

	affected, _ := r.RowsAffected()
	fmt.Println("insert affect row", affected)
	tx.Commit()
	return nil
}
